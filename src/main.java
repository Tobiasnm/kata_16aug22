import java.util.Scanner;
import java.util.regex.Pattern;

public class main {
    public static void main(String[] args) {
        String input;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type your password to check its strength!");
        input = scanner.nextLine();
        System.out.println(CheckPassword(input));
    }
    private static String CheckPassword(String password) {
        int strength = 0;
        if(password.length() < 6 || password.contains(" "))
            return "Password is invalid";
        if(Pattern.compile("[a-z]+").matcher(password).find())
            strength++;
        if(Pattern.compile("[A-Z]+").matcher(password).find())
            strength++;
        if(Pattern.compile("[0-9]+").matcher(password).find())
            strength++;
        if(Pattern.compile("[!#$%&'()*+,-./:;<=>?@\\[\\]^_`{|}~]+").matcher(password).find())
            strength++;
        if(password.length()>7)
            strength++;
        return(strength > 4 ? "Password is strong" : strength > 2 ? "Password is moderate" : "Password is weak");
    }
}
